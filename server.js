var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path'); //esta no se instala, se referencia

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// app.get('/', function(req,res){
// res.send('Hola mundo');
//});

app.get('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req,res){
  res.send('Hemos recibido su petición cambiada');
});

app.put('/', function(req,res){
  res.send('Hemos modificado su petición');
});

app.delete('/', function(req,res){
  res.send('Hemos eliminado su petición');
});

app.get('/clientes/:idcliente', function(req,res){
  res.send('Aqui tiene al cliente número ' + req.params.idcliente);
});

app.delete('/clientes/:idcliente', function(req,res){
  res.send('Hemos eliminado el cliente ' + req.params.idcliente);
});

app.put('/clientes/:idcliente', function(req,res){
  res.send('Hemos modificado el cliente ' + req.params.idcliente);
});
